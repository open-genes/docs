# Документация [Open Genes](https://open-genes.com/)

# [WIKI](https://gitlab.com/open-genes/docs/-/wikis/pages)
<div class="content" id="content-body">
<ul class="wiki-pages-list content-list">


<li>
Документы
<ul>
<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F-%D0%BF%D0%BE-%D0%BD%D0%B0%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D1%8E-%D0%B1%D0%B0%D0%B7%D1%8B">Инструкция по наполнению базы</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%9D%D0%BE%D0%B2%D1%8B%D0%BC-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC">Новым разработчикам</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0-%D0%B4%D0%BB%D1%8F-%D0%B8%D0%BD%D0%B2%D0%B5%D1%81%D1%82%D0%BE%D1%80%D0%BE%D0%B2-(EN)">Описание проекта для инвесторов (EN)</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5-%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D0%BE%D0%BD%D0%B0%D0%BB%D0%B0">Описание функционала</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%A0%D0%BE%D0%BB%D0%B8-%D0%B2-%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D0%B5-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0">Роли в команде проекта</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B7%D0%B0%D0%B4%D0%B0%D1%87-%D0%BF%D0%BE-%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D0%BC-%D0%B8-%D0%91%D0%94">Список задач по формам и БД</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%A1%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B0-%D0%BF%D0%BE-CMS">Справка по CMS</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B8-%D0%BD%D0%B0-%D0%BE%D1%82%D1%87%D0%B5%D1%82%D1%8B">Ссылки на отчеты</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%A2%D0%97-%D0%BF%D0%BE-%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D0%BC-%D0%B4%D0%BB%D1%8F-%D1%80%D1%83%D1%87%D0%BD%D0%BE%D0%B3%D0%BE-%D0%BD%D0%B0%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D1%8F-%D0%B1%D0%B0%D0%B7%D1%8B">ТЗ по формам для ручного наполнения базы</a>
<small>(markdown)</small>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D1%8B/%D0%A2%D0%B5%D1%85%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B5-%D0%BE%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0">Техническое описание проекта</a>
</li>


</ul>
</li>
<li>
Отчеты-по-релизам
<ul>
<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B5%D0%BB%D0%B8%D0%B7%D0%B0%D0%BC/19.09">19.09</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B5%D0%BB%D0%B8%D0%B7%D0%B0%D0%BC/19.10">19.10</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B5%D0%BB%D0%B8%D0%B7%D0%B0%D0%BC/19.11">19.11</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B5%D0%BB%D0%B8%D0%B7%D0%B0%D0%BC/19.12">19.12</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B5%D0%BB%D0%B8%D0%B7%D0%B0%D0%BC/20.01">20.01</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B5%D0%BB%D0%B8%D0%B7%D0%B0%D0%BC/20.02">20.02</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%9E%D1%82%D1%87%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE-%D1%80%D0%B5%D0%BB%D0%B8%D0%B7%D0%B0%D0%BC/20.03">20.03</a>
</li>
</ul>
</li>
<li>
Разработчикам
<ul>
<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC/API">API</a>
</li>
<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC/GitFlow">GitFlow</a>
</li>


</ul>
</li>
<li>
Разработчикам/Backend
<ul>
<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC/Backend/%D0%90%D1%80%D1%85%D0%B8%D1%82%D0%B5%D0%BA%D1%82%D1%83%D1%80%D0%B0-Backend">Архитектура Backend</a>
</li>

<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC/Backend/%D0%A1%D1%82%D1%80%D1%83%D0%BA%D1%82%D1%83%D1%80%D0%B0-%D0%B1%D0%B0%D0%B7%D1%8B-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85">Структура базы данных</a>
</li>


</ul>
</li>
<li>
Разработчикам/Frontend
<ul>
<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC/Frontend/%D0%90%D1%80%D1%85%D0%B8%D1%82%D0%B5%D0%BA%D1%82%D1%83%D1%80%D0%B0-Frontend">Архитектура Frontend</a>
</li>
<li>
<a href="https://gitlab.com/open-genes/docs/-/wikis/%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC/Frontend/%D0%9F%D0%BE%D0%B4%D0%B4%D0%B5%D1%80%D0%B6%D0%BA%D0%B0-%D0%B1%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80%D0%BE%D0%B2">Поддержка браузеров</a>
</li>
</ul>
</li>
</ul>
</div>
